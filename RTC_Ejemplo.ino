
//Hecho por Federico Capdeville
//Probado con el Blue Pill (STM32F103C8T6)

//Defino el RTC desde la compu, por puerto Serial y lo bajo al micro.
//Luego, envio los datos a traves del Serial por si los quiero ver en la compu

//Variables que uso para el RTC (puerto serie)
/* q ==> Testeo de que funcione la conexion
 * w ==> Leo del micro y muestro: año, mes, dia (fecha), dia (dia semanal)
 * e ==> Leo del micro y muestro: hora, minuto y segundo
 * f yyyy-mm-dd hh:mm# ==> Mando desde la compu para setear al RTC del micro, el # indica el final del string
 * r ==> Verifico y leo lo que mande de año-mes-dia-hora-minuto
 * 
 * Ejemplo de envio: f2018-11-01 16:30##    (envio por el hercules)
 */

#include <RTClock.h>
#include <TimeLib.h>
#define LED PC13

//Variables que uso para el RTC
RTClock rtc (RTCSEL_LSI);   //Si uso el RTCSEL_LSE, asume que entre PC15 y PC14 tengo un cristal de 32.768MHz ==> Debo usar RTCSEL_LSI para hacer pruebas

uint8 seteo_amd[17]; //Seteo año, mes, dia, hora y minutos
int a = 0, i = 0;
int ano, mes, dia, hora, minuto;
uint32_t tiempo = 0;

struct tm_t time_tm;


void setup() 
{  
  pinMode(LED, OUTPUT);      //Coloco un led en la pata PB12

  Serial.begin(9600);  
}


void loop() 
{
  while (Serial.available()) 
  {
//Cosas del RTC
    tiempo = rtc.getTime();

//Cosas del puerto Serial    
    uint8 input = Serial.read();
    uint8 escape = 0, datos_serie = 0;

//Blanqueo un par de variables    
    i = 0;
    escape = 0;
    seteo_amd[16] = 0;

    switch(input) 
    {
      case 'q':                                               //Variable de verificacion
        Serial.print("\n");
        Serial.println("Verificando la conexion.");
        break;

      case 'w':                                               //Envio año, mes, dia (fecha) y dia de la semana
        Serial.print("\n");

        Serial.print("Envio el año: ");
        Serial.print(year(tiempo));
        Serial.print("\n");

        Serial.print("Envio el mes: ");
        Serial.print(month(tiempo));
        Serial.print("\n");
        
        Serial.print("Envio el dia (fecha): ");
        Serial.print(day(tiempo));
        Serial.print("\n");

        Serial.print("Envio el dia (dia semanal): ");
        Serial.print(weekday(tiempo));
        Serial.print("\n");

        break;

      case 'e':                                               //Envio hora, minutos y segundos
        Serial.print("\n");

        Serial.print("Envio la hora: ");
        Serial.print(hour(tiempo));
        Serial.print("\n");

        Serial.print("Envio los minutos: ");
        Serial.print(minute(tiempo));
        Serial.print("\n");

        Serial.print("Envio los segundos: ");
        Serial.print(second(tiempo));
        Serial.print("\n");

        break;

      case 'r':                                               //Reenvio lo que mande desde la compu, para chequeo
        Serial.print("\n");
        for (int j = 0; j < 16; j++)
          Serial.write(seteo_amd[j]);
        Serial.print("\n");
        break;
    }

    while ((input == 'f') && (escape == 0))                   //Voy leyendo los datos hasta que recibaun #, indicando el final
    {
      datos_serie = Serial.read();

      seteo_amd[i] = datos_serie;
      i++;
      
      if (seteo_amd[16] == '#')                               //Si termino, que guarde lo enviado en el RTC
      {
        ano = (seteo_amd[0]-48) * 1000 + (seteo_amd[1]-48) * 100 + (seteo_amd[2]-48) * 10 + (seteo_amd[3]-48);
        mes = (seteo_amd[5]-48) * 10 + (seteo_amd[6]-48);
        dia = (seteo_amd[8]-48) * 10 + (seteo_amd[9]-48);
        hora = (seteo_amd[11]-48) * 10 + (seteo_amd[12]-48);
        minuto = (seteo_amd[14]-48) * 10 + (seteo_amd[15]-48);

        time_tm.year = ano-1970;
        time_tm.month = mes;
        time_tm.day = dia;
        time_tm.pm = 0;
        time_tm.hour = hora;
        time_tm.minute = minuto;
        time_tm.second = 0;
        
        rtc.setTime(time_tm);
        
        escape = 1;                                           //Le indico que termino la cadena de elementos enviados
      }
    }
  }  
}
